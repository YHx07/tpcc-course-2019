#pragma once

#include "fiber.hpp"

namespace fiber {

//////////////////////////////////////////////////////////////////////

class Scheduler {
 public:
  Scheduler(size_t threads = 0);

  void Submit(FiberRoutine routine);
  size_t ThreadCount() const;
  void Shutdown();
};

//////////////////////////////////////////////////////////////////////

Fiber* GetCurrentFiber();

//////////////////////////////////////////////////////////////////////

void Spawn(FiberRoutine routine);
void Yield();
void Terminate();

}  // namespace fiber
