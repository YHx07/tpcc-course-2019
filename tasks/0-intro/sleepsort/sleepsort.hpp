#pragma once

#include <vector>

namespace solutions {

// Genius sorting algorithms: Sleep sort
// Invented by anonymous 4chan user
// https://archive.tinychan.org/read/prog/1295544154

// You may assume that 0 <= ints[i] <= 100
void SleepSort(std::vector<int>& ints);

}  // namespace solutions
