cmake_minimum_required(VERSION 3.9)

add_subdirectory(0-intro)
add_subdirectory(1-mutex)
add_subdirectory(2-condvar)
add_subdirectory(3-fiber)
add_subdirectory(4-maps)
add_subdirectory(5-cache)
add_subdirectory(6-lockfree)

