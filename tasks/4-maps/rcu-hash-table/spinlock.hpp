#pragma once

#include <twist/stdlike/atomic.hpp>
#include <twist/threading/spin_wait.hpp>

namespace solutions {

class SpinLock {
 public:
  void Lock() {
    // Your code goes here
  }

  void Unlock() {
    // Your code goes here
  }

  // BasicLockable

  void lock() {  // NOLINT
    Lock();
  }

  void unlock() {  // NOLINT
    Unlock();
  }
};

}  // namespace solutions
