#pragma once

#include <twist/threading/tls.hpp>
#include <twist/stdlike/atomic.hpp>
#include <twist/support/noncopyable.hpp>

#include <cassert>
#include <functional>

namespace solutions {

template <typename T>
class ThreadLocal : twist::NonCopyable {
  struct ValueNode {
    T value_;
    ValueNode* next_{nullptr};
  };

  using ValueInitializer = std::function<void(T&)>;

 public:
  ThreadLocal() : ThreadLocal([](T& /*target*/) {}) {
  }

  explicit ThreadLocal(T default_value)
      : ThreadLocal([default_value](T& target) { target = default_value; }) {
  }

  explicit ThreadLocal(ValueInitializer initializer)
      : value_initializer_(initializer) {
    tls_key_ = twist::th::AcquireKey();
  }

  ~ThreadLocal() {
    ClearValuesList();
    twist::th::ReleaseKey(tls_key_);
  }

  // Access thread local value

  T& operator*() {
    auto* thread_value_node = AccessThreadValueNode();
    return thread_value_node->value_;
  }

  T* operator->() {
    auto* thread_value_node = AccessThreadValueNode();
    return &(thread_value_node->value_);
  }

  // Iterate over values of all threads

  class Iterator {
   public:
    Iterator(ValueNode* head) : current_(head) {
    }

    bool IsValid() const {
      return current_ != nullptr;
    }

    T& operator*() {
      return current_->value_;
    }

    T* operator->() {
      return &current_->value_;
    }

    bool operator==(const Iterator& that) const {
      return current_ == that.current_;
    }

    bool operator!=(const Iterator& that) const {
      return !(*this == that);
    }

    void operator++() {
      assert(IsValid());
      current_ = current_->next_;
    }

   private:
    ValueNode* current_;
  };

  // support range-based for loops

  Iterator begin() {  // NOLINT
    return Iterator{thread_values_head_.load()};
  }

  Iterator end() {  // NOLINT
    return Iterator{nullptr};
  }

  // TODO: const begin/end

 private:
  ValueNode* AccessThreadValueNode() {
    // slot in TLS stores pointer to value node
    void** slot = (void**)twist::th::AccessSlot(tls_key_);

    if (*slot == nullptr) {
      // slow path
      *slot = (void*)AllocateThreadValueNodeForThisThread();
    }

    return (ValueNode*)*slot;
  }

  void AddNodeToValuesList(ValueNode* thread_value_node) {
    thread_value_node->next_ = thread_values_head_.load();
    while (!thread_values_head_.compare_exchange_weak(thread_value_node->next_,
                                                      thread_value_node)) {
      // retry
    }
  }

  ValueNode* AllocateThreadValueNodeForThisThread() {
    ValueNode* thread_value_node = new ValueNode();
    value_initializer_(thread_value_node->value_);

    AddNodeToValuesList(thread_value_node);
    return thread_value_node;
  }

  void ClearValuesList() {
    ValueNode* current = thread_values_head_.load();
    while (current != nullptr) {
      ValueNode* next = current->next_;
      delete current;
      current = next;
    }
  }

 private:
  ValueInitializer value_initializer_;
  twist::atomic<ValueNode*> thread_values_head_{nullptr};
  size_t tls_key_;
};

}  // namespace solutions
