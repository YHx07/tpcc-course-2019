#pragma once

#include <twist/stdlike/mutex.hpp>
#include <twist/stdlike/condition_variable.hpp>
#include <twist/support/locking.hpp>

#include <cstddef>

namespace solutions {

class CyclicBarrier {
 public:
  explicit CyclicBarrier(size_t num_threads) {
  }

  void PassThrough() {
    // Your code goes here
  }
};

}  // namespace solutions

