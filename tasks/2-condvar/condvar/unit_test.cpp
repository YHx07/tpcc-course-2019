#include "condvar.hpp"

#include <twist/support/locking.hpp>

#include <twist/test_framework/test_framework.hpp>
#include <twist/threading/test.hpp>

#include <twist/threading/stdlike.hpp>

#include <atomic>
#include <chrono>

TEST_SUITE(CondVar) {
  SIMPLE_T_TEST(NotifyOne) {
    twist::th::mutex mutex;
    solutions::ConditionVariable condvar;

    bool pass{false};

    auto wait_routine = [&]() {
      // replacement for verbose std::unique_lock<std::mutex> lock(mutex)
      auto lock = twist::LockUnique(mutex);

      while (!pass) {
        condvar.Wait(lock);
      }
    };

    twist::th::thread t(wait_routine);

    twist::th::this_thread::sleep_for(
        std::chrono::milliseconds(250));

    {
      auto lock = twist::LockUnique(mutex);
      pass = true;
      condvar.NotifyOne();
    }

    t.join();
  }

  SIMPLE_T_TEST(BlockingWait) {
    int state = 0;
    twist::th::mutex mutex;
    solutions::ConditionVariable condvar;

    condvar.NotifyOne();
    condvar.NotifyAll();

    auto wait_routine = [&]() {
      auto lock = twist::LockUnique(mutex);
      ASSERT_EQ(state, 0);
      condvar.Wait(mutex);
      ASSERT_EQ(state, 1);
    };

    twist::th::thread t(wait_routine);

    twist::th::this_thread::sleep_for(std::chrono::seconds(1));
    ASSERT_EQ(state, 0);

    {
      auto lock = twist::LockUnique(mutex);
      state = 1;
      condvar.NotifyOne();
    }

    t.join();
  }

  SIMPLE_T_TEST(NotifyAll) {
    twist::th::mutex mutex;
    solutions::ConditionVariable condvar;
    bool pass{false};

    auto wait_routine = [&]() {
      auto lock = twist::LockUnique(mutex);

      while (!pass) {
        condvar.Wait(lock);
      }
    };

    twist::th::thread t1(wait_routine);
    twist::th::thread t2(wait_routine);

    twist::th::this_thread::sleep_for(
        std::chrono::milliseconds(250));

    {
      auto lock = twist::LockUnique(mutex);
      pass = true;
      condvar.NotifyAll();
    }

    t1.join();
    t2.join();
  }

  SIMPLE_T_TEST(NotifyManyTimes) {
    static const size_t kIterations = 1000 * 1000;

    solutions::ConditionVariable condvar;
    for (size_t i = 0; i < kIterations; ++i) {
      condvar.NotifyOne();
    }
  }
}

RUN_ALL_TESTS()

