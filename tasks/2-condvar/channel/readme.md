## Блокирующая очередь / канал

Реализуйте очередь (канал) ограниченной емкости с помощью условных переменных.

Очередь – механизм передачи данных между потоками.

Пример использования очереди – доставка и распределение задач между воркерами в пуле потоков.

Интерфейс очереди:

- Конструктор принимает `capacity` – емкость очереди.

- Метод `void Send(T item)` добавляет элемент в конец очереди. Если очередь закрыта вызовом `Close`, то метод `Send` должен бросить специальное исключение `ChannelClosed`. Если достигнута `capacity` очереди, и добавить элемент нельзя, то метод `Send` должен заблокироваться до тех пор, пока не появится свободный слот либо пока очередь не закроют для вставок вызовом `Close`. 

- Метод `void Close()` закрывает очередь для новых `Send`-ов, прерывает `Send`-ы, заблокированные на вставке в заполненную очередь, и разблокирует `Recv`-ы, ждущие элементов на пустой очереди.

- Метод `bool Recv(T& item)` извлекает элемент из головы очереди. Вызов завершается без ожидания, если очередь не пуста (при этом она может быть закрыта для вставок). Если очередь пуста, то вызов блокируется до появления нового элемента либо до вызова `Close`. Если элемент успешно извлечен, то `Recv` записывает его по переданной ссылке и возвращает `true`. Если же `Recv` увидел пустую закрытую очередь или был разблокирован вызовом `Close`, то он возвращает `false`.

Приведенные выше правила могут показаться сложными, но они лишь фиксируют здравый смысл: ровно такого поведения вы бы сами ожидали от очереди.

Закрытие очереди – необратимая операция, возобновить вставки в закрытую очередь нельзя. При этом пользователь может вызвать метод `Close` несколько раз.

Ёмкость не влияет на количество элементов, которое можно передать через очередь. Этот параметр нужен, чтобы ограничить потребление памяти и затормозить слишком быстрых продьюсеров.

Для лучшего понимания интерфейса очереди рекомендуется разобрать юнит-тесты к задаче.

---

Очередь передает элементы шаблонного типа. Будьте готовы к тому, что у этого типа может не быть копирующего конструктора / оператора присваивания.

---

### Golang

В языке [Go](https://golang.org/) описанный выше примитив называется [_буферизированным_](https://gobyexample.com/channel-buffering) [_каналом_](https://gobyexample.com/channels).

Каналы в Go – это наиболее идеоматичный способ синхронизации [_горутин_](https://tour.golang.org/concurrency/1) – так в Go называются файберы. 

Например, семафоры из соседней задачи тривиально выражаются через каналы.


