#include "logger.hpp"

#include <twist/fault/adversary.hpp>
#include <twist/fault/lockfree.hpp>

#include <twist/test_framework/test_framework.hpp>
#include <twist/threading/test.hpp>

#include <twist/threading/spin_wait.hpp>

#include <twist/test_utils/barrier.hpp>
#include <twist/test_utils/executor.hpp>

#include <twist/support/locking.hpp>
#include <twist/support/random.hpp>
#include <twist/support/time.hpp>

#include <chrono>
#include <cstdlib>
#include <unordered_map>

using logging::LogEvent;
using logging::ILogWriter;
using logging::ILogWriterPtr;
using logging::LogEvents;

//////////////////////////////////////////////////////////////////////

class RateLimiter {
 public:
  void Pass() {
    ++count_;
    if (count_ % 111 == 0) {
      std::this_thread::sleep_for(
        std::chrono::microseconds(1));
    }
  }

 private:
  size_t count_{0};
};

//////////////////////////////////////////////////////////////////////

// Test-and-TAS spinlock

class SpinLock {
 public:
  void lock() {  // NOLINT
    while (locked_.exchange(true)) {
      twist::SpinWait spin_wait;
      while (locked_.load()) {
        spin_wait();
      }
    }
  }

  void unlock() {  // NOLINT
    locked_.store(false);
  }

 private:
  std::atomic<bool> locked_{false};
};

//////////////////////////////////////////////////////////////////////

class LogWriter : public ILogWriter {
 public:
  void Write(const LogEvents &events) override {
    event_count_ += events.size();

    for (const auto& event : events) {
      Process(event);
    }

    RandomDelay();
  }

  size_t EventCount() const {
    return event_count_;
  }

  size_t ThisThreadLastWrite() {
    auto lock = twist::LockUnique(spinlock_);
    auto last = last_per_thread_.find(std::this_thread::get_id());
    return last->second;
  }

 private:
  void Process(const LogEvent& event) {
    size_t event_idx = std::atoi(event.message_.c_str());

    auto lock = twist::LockUnique(spinlock_);
    auto prev = last_per_thread_.find(event.id_);

    if (prev != last_per_thread_.end()) {
      ASSERT_TRUE_M(prev->second + 1 == event_idx, "Per-thread fifo ordering violated");
    }
    last_per_thread_[event.id_] = event_idx;
  }

  void RandomDelay() {
    auto delay = std::chrono::microseconds(
      twist::RandomUInteger(100, 1000));
    std::this_thread::sleep_for(delay);
  }

 private:
  std::atomic<size_t> event_count_{0};
  std::unordered_map<std::thread::id, size_t> last_per_thread_;
  SpinLock spinlock_;
};

//////////////////////////////////////////////////////////////////////

void FifoStressTest(TTestParameters parameters) {
  size_t threads = parameters.Get(0);
  size_t writes = parameters.Get(1);

  auto writer = std::make_shared<LogWriter>();
  logging::Logger logger(writer);

  twist::OnePassBarrier start_barrier(threads);

  auto worker = [&]() {
    start_barrier.PassThrough();

    auto adversary = twist::fault::GetAdversary();

    RateLimiter rate_limiter;

    adversary->Enter();

    for (size_t i = 0; i < writes; ++i) {
      rate_limiter.Pass();
      logger.Log(std::to_string(i));
      adversary->ReportProgress();

      if (i % 123 == 0) {
        logger.Synchronize();
        auto last_write = writer->ThisThreadLastWrite();
        ASSERT_TRUE_M(last_write == i, "Synchronize doesn't work");
      }
    }

    adversary->Exit();

    logger.Synchronize();
  };

  twist::ScopedExecutor executor;
  for (size_t i = 0; i < threads; ++i) {
    executor.Submit(worker);
  }
  executor.Join();

  ASSERT_EQ(writer->EventCount(), threads * writes);

  logger.Stop();
}

//////////////////////////////////////////////////////////////////////

T_TEST_CASES(FifoStressTest)
  .TimeLimit(std::chrono::minutes(1))
  .Case({1, 100000})
  .Case({5, 100000})
  .TimeLimit(std::chrono::minutes(2))
  .Case({10, 50000});

int main() {
  twist::fault::SetAdversary(
    twist::fault::CreateLockFreeAdversary());
  RunTests(ListAllTests());
}

