#include "toyalloc.hpp"

#include <twist/support/random.hpp>

#include <twist/fault/inject_fault.hpp>
#include <twist/fault/lockfree.hpp>
#include <twist/fault/nop.hpp>

#include <twist/ratelimit/quiescent.hpp>

#include <twist/test_framework/test_framework.hpp>
#include <twist/threading/test.hpp>
#include <twist/threading/spin_wait.hpp>

#include <twist/test_utils/barrier.hpp>
#include <twist/test_utils/executor.hpp>

#include <algorithm>
#include <atomic>
#include <chrono>
#include <cstdlib>
#include <vector>

////////////////////////////////////////////////////////////////////////////////

template <typename T>
std::atomic<T>* AsAtomic(void* addr) {
  return reinterpret_cast<std::atomic<T>*>(addr);
}

void PutCanary(void* addr, size_t thread) {
  AsAtomic<size_t>(addr)->store(thread);
}

void CheckCanary(void *addr, size_t thread) {
  ASSERT_EQ(AsAtomic<size_t>(addr)->load(), thread);
}

namespace stress {
  class Tester {
   public:
    Tester(const TTestParameters& parameters)
        : parameters_(parameters),
          start_barrier_(parameters.Get(0)),
          alloc_limiter_(parameters.Get(0), std::chrono::microseconds(1)) {
    }

    // One-shot
    void Run() {
      twist::ScopedExecutor executor;

      for (size_t t = 0; t < parameters_.Get(0); ++t) {
        executor.Submit(&Tester::ThreadAllocate, this, t);
      }

      executor.Join();
      alloc_limiter_.Stop();

      std::cout << "Quiescent points: " << alloc_limiter_.QuiescentPointCount() << std::endl;
    }

   private:
    void ThreadAllocate(size_t thread_index) {
      start_barrier_.PassThrough();

      auto adversary = twist::fault::GetAdversary();

      adversary->Enter();

      size_t batch_limit = parameters_.Get(1);

      size_t iterations = parameters_.Get(2);
      for (size_t i = 0; i < iterations; ++i) {
        std::vector<void*> allocated;

        size_t batch_size = twist::RandomUInteger(batch_limit);

        // Allocate
        for (size_t j = 0; j < batch_size; ++j) {
          alloc_limiter_.Enter();
          void* addr = toyalloc::Allocate();
          alloc_limiter_.Exit();
          adversary->ReportProgress();

          ASSERT_TRUE(addr != nullptr);
          PutCanary(addr, thread_index);
          allocated.push_back(addr);
        }

        // Release
        for (void* addr : allocated) {
          CheckCanary(addr, thread_index);
          alloc_limiter_.Enter();
          toyalloc::Free(addr);
          alloc_limiter_.Exit();
          adversary->ReportProgress();
        }
      }

      adversary->Exit();
    }

   private:
    TTestParameters parameters_;
    twist::OnePassBarrier start_barrier_;
    twist::ratelimit::QuiescentRateLimiter alloc_limiter_;
  };

};

void StressTest(TTestParameters parameters) {
  stress::Tester(parameters).Run();
}

// Parameters: alloc threads, alloc batch limit, forks

T_TEST_CASES(StressTest)
  .TimeLimit(std::chrono::seconds(30))
  .Case({1, 100, 1000})
  .Case({5, 5, 10000})
  .Case({5, 10, 5000})
  .TimeLimit(std::chrono::minutes(1))
  .Case({5, 100, 1000});

////////////////////////////////////////////////////////////////////////////////

void InitializeAllocator() {
  static const size_t kArenaPages = 1024 * 10;
  auto arena = twist::MmapAllocation::AllocatePages(kArenaPages);

  twist::th::RunInThreadingContext([&]() {
    toyalloc::Init(std::move(arena));
  });
}

int main() {
  InitializeAllocator();
  twist::fault::SetAdversary(twist::fault::CreateLockFreeAdversary());
  RunTests(ListAllTests());
  return 0;
}
