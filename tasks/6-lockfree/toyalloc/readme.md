# Аллокатор

В этой задаче вы должны написать игрушечный lock-free аллокатор. Чуть точнее – лок-фри кэш свободных блоков для аллокатора.

Гарантия lock-freedom позволяет вам не думать о `fork`: 

По определению lock-freedom любая пауза (или исчезновение, как в случае с `fork`) любого из потоков в любой точке `Allocate` или `Free` не может привести к блокировке потока, который выполнил `fork` и хочет аллоцировать память в дочернем процессе. 

Для решения ABA вы можете выбрать любой известный вам метод.

## Quiescent points

Гарантируется, что во время исполнения теста будут моменты, когда ни один из потоков не находится внутри аллокации. Для этого потоки перед обращением к аллокатору проходят через специальный rate limiter, который их периодически вытесняет.

##  Lock-freedom 

Стресс-тесты в этой задаче не делают форков!

Вместо этого тесты напрямую проверяют выполнение гарантии _lock-freedom_:

- Специальный adversary через fault injection перехватывает обращения к атомикам и периодически паркует потоки, которые обращаются к аллокатору.
- Потоки уведомляют adversary о завершении своей операции с помощью метода `ReportProgress`. Adversary в ответ на это событие может разбудить один из припаркованных потоков.
