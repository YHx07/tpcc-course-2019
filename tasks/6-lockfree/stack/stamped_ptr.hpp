#pragma once

#include <twist/stdlike/atomic.hpp>

#include <twist/support/assert.hpp>

#include <cstdlib>

//////////////////////////////////////////////////////////////////////

// Pointer + Stamp

template <typename T>
struct StampedPtr {
  T* raw_ptr;
  size_t stamp;

  explicit operator bool() const {
    return raw_ptr != nullptr;
  }

  T* Ptr() const {
    return raw_ptr;
  }

  size_t Stamp() const {
    return stamp;
  }

  StampedPtr IncrementStamp() const {
    return StampedPtr{raw_ptr, stamp + 1};
  }

  StampedPtr DecrementStamp() const {
    VERIFY(stamp > 0, "Cannot decrement zero stamp")
    return StampedPtr{raw_ptr, stamp - 1};
  }

  T* operator->() const {
    return raw_ptr;
  }

  T& operator*() const {
    return *raw_ptr;
  }
};

//////////////////////////////////////////////////////////////////////

// 48-bit pointer + 16-bit stamp packed in single machine word

// Usage examples:
// asp.Store({raw_ptr, 42});
// auto ptr = asp.Load();
// if (ptr) { ... }
// ptr->Foo();
// asp.CompareExchange(ptr, {ptr.raw_ptr, ptr.stamp + 1});

template <typename T>
class AtomicStampedPtr {
  using StampedPtr = ::StampedPtr<T>;

 public:
  static const size_t kMaxStamp = 1 << 16;

 public:
  AtomicStampedPtr(T* ptr = nullptr) {
    Store({ptr, 0});
  }

  AtomicStampedPtr(StampedPtr ptr) {
    Store(ptr);
  }

  void Store(StampedPtr ptr,
             std::memory_order order = std::memory_order_seq_cst) {
    packed_ptr_.store(Pack(ptr), order);
  }

  StampedPtr Load(std::memory_order order = std::memory_order_seq_cst) const {
    return Unpack(packed_ptr_.load(order));
  }

  StampedPtr Exchange(StampedPtr target,
                      std::memory_order order = std::memory_order_seq_cst) {
    auto prev = packed_ptr_.exchange(Pack(target), order);
    return Unpack(prev);
  }

  bool CompareExchangeWeak(
      StampedPtr& expected, StampedPtr desired,
      std::memory_order order = std::memory_order_seq_cst) {
    uintptr_t expected_packed = Pack(expected);
    bool succeeded = packed_ptr_.compare_exchange_weak(expected_packed,
                                                       Pack(desired), order);
    if (!succeeded) {
      expected = Unpack(expected_packed);
    }
    return succeeded;
  }

 private:
  static uintptr_t ResetHigh16Bits(uintptr_t ptr) {
    return (ptr << 16) >> 16;
  }

  static uintptr_t Pack(StampedPtr stamped_ptr) {
    return ResetHigh16Bits((uintptr_t)stamped_ptr.raw_ptr) |
           (stamped_ptr.stamp << 48);
  }

  // For x86-64 pointers have a canonical form in which the upper 16 bits
  // are equal to bit 47 (zero-based)

  static int GetBit(uintptr_t value, size_t index) {
    return (value >> index) & 1;
  }

  static uintptr_t ToCanonicalForm(uintptr_t ptr) {
    if (GetBit(ptr, 47)) {
      return ptr | ((uintptr_t)0xFFFF << 48);
    } else {
      return ResetHigh16Bits(ptr);
    }
  }

  static size_t GetStamp(uintptr_t packed_ptr) {
    return packed_ptr >> 48;
  }

  static T* GetRawPtr(uintptr_t packed_ptr) {
    return (T*)ToCanonicalForm(packed_ptr);
  }

  static StampedPtr Unpack(uintptr_t packed_ptr) {
    return {GetRawPtr(packed_ptr), GetStamp(packed_ptr)};
  }

 private:
  twist::atomic<uintptr_t> packed_ptr_;
};
