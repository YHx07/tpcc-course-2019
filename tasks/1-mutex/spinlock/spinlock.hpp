#pragma once

#include "atomics.hpp"

#include <twist/threading/stdlike.hpp>
#include <twist/threading/spin_wait.hpp>

namespace solutions {

// Simple Test-and-Set (TAS) spinlock

class SpinLock {
 public:
  void Lock() {
    twist::SpinWait spin_wait;
    while (AtomicExchange(&locked_, 1)) {
      spin_wait();
    }
  }

  bool TryLock() {
    return false;  // Not implemented
  }

  void Unlock() {
    AtomicStore(&locked_, 0);
  }

 private:
  volatile std::int64_t locked_ = 0;
};

}  // namespace solutions
