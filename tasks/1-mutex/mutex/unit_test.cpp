#include "mutex.hpp"

#include <twist/test_framework/test_framework.hpp>
#include <twist/threading/test.hpp>

#include <twist/test_utils/executor.hpp>
#include <twist/test_utils/mutex_tester.hpp>

#include <thread>

TEST_SUITE(UnitTest) {
  SIMPLE_T_TEST(LockUnlock) {
    solutions::Mutex mutex;
    mutex.Lock();
    mutex.Unlock();
  }

  SIMPLE_T_TEST(SequentialLockUnlock) {
    solutions::Mutex mutex;
    mutex.Lock();
    mutex.Unlock();
    mutex.Lock();
    mutex.Unlock();
  }

  SIMPLE_T_TEST(SharedMemoryValue) {
    // Тестируем использование общей ячейки памяти на разные инстансы локов
    solutions::Mutex mutex;
    mutex.Lock();

    solutions::Mutex mutex2;
    mutex2.Lock();  // тут не должно блокироваться
    mutex2.Unlock();

    mutex.Unlock();
  }

  SIMPLE_T_TEST(ConcurrentLock) {
    twist::MutexTester<solutions::Mutex> mutex;

    volatile int counter = 0;

    auto routine = [&mutex, &counter]() {
      mutex.Lock();
      twist::th::this_thread::sleep_for(std::chrono::milliseconds(100));
      counter++;
      mutex.Unlock();
    };

    twist::ScopedExecutor executor;
    executor.Submit(routine);
    executor.Submit(routine);
    executor.Join();

    ASSERT_EQ(2, counter);
  }
}

RUN_ALL_TESTS()
