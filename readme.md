# Теория и практика concurrency

Добро пожаловать в репозиторий курса!

Прочтите [Getting Started](https://gitlab.com/Lipovsky/tpcc-course-2019/blob/master/docs/getting-started.md) для начала работы.

- [Текущие результаты](https://docs.google.com/spreadsheets/d/1ZLmegDQ19CV4QfbFpiAqW_BNrLMW1Tlq4HYUeXptvh4/edit?usp=sharing)
- [Приложение](http://35.228.4.33:5000/)
- [Дедлайны](https://gitlab.com/Lipovsky/tpcc-course-2019/blob/master/.deadlines.yml)

## Навигация

В `docs` вы можете найти инструкции, правила, стайлгайд, FAQ и другие полезные документы.

В `lectures` – план курса и расписание лекций, ссылки, статьи и т.п.

В `tasks` – домашние задания

В `library` – библиотеку для тестирования решений

В `client` – консольный клиент для работы с задачами.

Остальные директории – служебные.
